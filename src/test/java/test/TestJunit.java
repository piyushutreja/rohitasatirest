package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import demo.MessageHandler;
import demo.MessageProducer;

public class TestJunit {
String msg = "helloWorld";
String msg2 = "Second message";
String msg3 = "Third message";
@Autowired
MessageProducer messageProducer;
@Autowired
MessageHandler messageHandler ;

@Test
public void testSendMessage() throws Exception
{
messageProducer.sendMessage(msg);

assertEquals(msg, messageHandler.processMsg(msg));
messageProducer.sendMessage(msg2);
assertEquals(msg2, messageHandler.processMsg(msg2));
messageProducer.sendMessage(msg3);
assertEquals(msg3, messageHandler.processMsg(msg3));
 
}
	
}
