package demo;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jms.core.JmsTemplate;
 
import org.springframework.stereotype.Service;

 
@Service
public class MessageProducer  {
	private static final Logger logger = LoggerFactory.getLogger(MessageProducer.class);
	 
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    Pojo ob ;
     
    public void sendMessage(Object msg) throws Exception {
    	
    	
        
        logger.info("============= Sending " + msg);
        logger.info("============= Sending " + this.jmsTemplate.getDefaultDestination());
        System.out.println("Rohitasati008"+(this.jmsTemplate.getDefaultDestination()));
        
        //System.out.println((this.jmsTemplate.getDefaultQueue()));
        this.jmsTemplate.convertAndSend("testQueue", new Pojo(12,"rohit"));
        this.jmsTemplate.convertAndSend("testQueue", msg);
    }
}
