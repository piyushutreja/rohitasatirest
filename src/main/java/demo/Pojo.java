package demo;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Pojo {
public Pojo(int rating, String name) {
		super();
		this.rating = rating;
		this.name = name;
	}
int rating;
String name ;
public int getRating() {
	return rating;
}
public void setRating(int rating) {
	this.rating = rating;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
