package demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
 
@Component
public class MessageHandler {
	 private static final Logger logger = LoggerFactory.getLogger(MessageHandler.class);
	 
     @JmsListener(destination = "testQueue")
     public String processMsg(String msg) {
         logger.info("============= Received " + msg);
         return msg ;
     }
}
